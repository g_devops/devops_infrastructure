#!/bin/bash

# Установка Yandex Cloud CLI
curl -sSL https://storage.yandexcloud.net/yandexcloud-yc/install.sh | bash
echo 'source /home/user123/yandex-cloud/completion.zsh.inc' >>  ~/.zshrc

# Обновление и установка Docker
echo 'Starting remote-exec provisioning...'
sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository 'deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable'
sudo apt-get update
sudo apt-get install -y docker-ce docker.io
sudo apt-get install -y docker.io
sudo usermod -aG docker user123
sudo usermod -aG docker $USER
sudo systemctl enable docker
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
docker-compose --version

# Вход в Yandex Container Registry
curl -H Metadata-Flavor:Google 169.254.169.254/computeMetadata/v1/instance/service-accounts/default/token | cut -f1 -d',' | cut -f2 -d':' | tr -d '"' | docker login --username iam --password-stdin cr.yandex

# Запуск docker-compose
echo 'Running docker-compose up...'
sudo docker-compose -f /home/user123/docker-compose.yml --env-file /home/user123/.env up -d > /home/user123/docker-compose.log 2>&1
echo 'Finished remote-exec provisioning. Check /home/user123/docker-compose.log for details.'
