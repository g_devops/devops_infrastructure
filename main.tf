terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = ">= 0.47.0"
    }
  }
}

provider "yandex" {
  zone                     = local.zone
  folder_id                = local.target_folder_id
  service_account_key_file = "./authorized_key.json"
}

module "registry" {
  source             = "./modules/registry"
  registry_name      = var.registry_name
  target_folder_id   = var.target_folder_id
}

module "service_account" {
  source             = "./modules/service_account"
  sa_name            = var.sa_name
  target_folder_id   = var.target_folder_id
}

module "network" {
  source             = "./modules/network"
  network_name       = var.network_name
  subnet_name        = var.subnet_name
  zone               = var.zone
}

module "compute_instance" {
  source             = "./modules/compute_instance"
  vm_name            = var.vm_name
  image_id           = var.image_id
  username           = var.username
  ssh_key_path       = var.ssh_key_path
  zone               = var.zone
  subnet_id          = module.network.subnet_id
  service_account_id = module.service_account.service_account_id
}

output "vm_public_ip" {
  value = module.compute_instance.vm_public_ip
}