terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
    }
  }
}

resource "yandex_container_registry" "vreg" {
  name       = var.registry_name
  folder_id  = var.target_folder_id
  labels     = {
    my-label = "my-label-value"
  }
}
