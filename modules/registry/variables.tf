variable "registry_name" {
  description = "Name of the registry"
  type        = string
}

variable "target_folder_id" {
  description = "ID of the target folder"
  type        = string
}
