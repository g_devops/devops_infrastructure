terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
    }
  }
}

resource "yandex_vpc_network" "docker-vm-network" {
  name = var.network_name
}

resource "yandex_vpc_subnet" "docker-vm-network-subnet-a" {
  name           = var.subnet_name
  zone           = var.zone
  v4_cidr_blocks = ["192.168.1.0/24"]
  network_id     = yandex_vpc_network.docker-vm-network.id
}

output "subnet_id" {
  value = yandex_vpc_subnet.docker-vm-network-subnet-a.id
}
