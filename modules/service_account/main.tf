terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
    }
  }
}

resource "yandex_iam_service_account" "registry-sa" {
  name      = var.sa_name
  folder_id = var.target_folder_id
}

resource "yandex_resourcemanager_folder_iam_member" "registry-sa-role-images-puller" {
  folder_id = var.target_folder_id
  role      = "container-registry.images.puller"
  member    = "serviceAccount:${yandex_iam_service_account.registry-sa.id}"
}

output "service_account_id" {
  value = yandex_iam_service_account.registry-sa.id
}
