variable "sa_name" {
  description = "Name of the service account"
  type        = string
}

variable "target_folder_id" {
  description = "ID of the target folder"
  type        = string
}
