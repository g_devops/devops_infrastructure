variable "vm_name" {
  description = "Name of the virtual machine"
  type        = string
}

variable "image_id" {
  description = "ID of the image"
  type        = string
}

variable "username" {
  description = "Username for SSH"
  type        = string
}

variable "ssh_key_path" {
  description = "Path to SSH key"
  type        = string
}

variable "zone" {
  description = "Zone"
  type        = string
}

variable "subnet_id" {
  description = "Subnet ID"
  type        = string
}

variable "service_account_id" {
  description = "Service account ID"
  type        = string
}
