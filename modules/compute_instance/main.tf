terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
    }
  }
}

resource "yandex_compute_disk" "boot-disk" {
  name     = "bootvmdisk"
  type     = "network-hdd"
  zone     = var.zone
  size     = "10"
  image_id = var.image_id
}

resource "yandex_compute_instance" "vm-vv" {
  name               = var.vm_name
  platform_id        = "standard-v3"
  zone               = var.zone
  service_account_id = var.service_account_id

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    disk_id = yandex_compute_disk.boot-disk.id
  }

  network_interface {
    subnet_id = var.subnet_id
    nat       = true
  }

  metadata = {
    user-data = <<EOF
#cloud-config
users:
  - name: ${var.username}
    groups: sudo
    shell: /bin/bash
    sudo: 'ALL=(ALL) NOPASSWD:ALL'
    ssh-authorized-keys:
      - ${file("${var.ssh_key_path}.pub")}
EOF
  }

  connection {
    type        = "ssh"
    user        = var.username
    private_key = file(var.ssh_key_path)
    host        = yandex_compute_instance.vm-vv.network_interface.0.nat_ip_address
  }

  provisioner "file" {
    source      = "./scripts/setup_instance.sh"
    destination = "/home/${var.username}/setup_instance.sh"
  }

  provisioner "file" {
    source      = "./run/docker-compose.yml"
    destination = "/home/${var.username}/docker-compose.yml"
  }

  provisioner "file" {
    source      = "./run/.env"
    destination = "/home/${var.username}/.env"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /home/${var.username}/setup_instance.sh",
      "sudo /home/${var.username}/setup_instance.sh"
    ]
  }
}

output "vm_public_ip" {
  value = yandex_compute_instance.vm-vv.network_interface[0].nat_ip_address
}
