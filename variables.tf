variable "zone" {
  default = "ru-central1-b"
}

variable "username" {
  type    = string
  description = "Username for the instance"
}


variable "ssh_key_path" {
  type    = string
  description = "Path to SSH private key"
}

variable "target_folder_id" {
  type    = string
  description = "Target folder ID"
}

variable "registry_name" {
  type    = string
  description = "Name of the container registry"
}

variable "sa_name" {
  type    = string
  description = "Name of the service account"
}

variable "network_name" {
  type    = string
  description = "Name of the VPC network"
}

variable "subnet_name" {
  type    = string
  description = "Name of the subnet"
}

variable "vm_name" {
  type    = string
  description = "Name of the VM instance"
}

variable "image_id" {
  type    = string
  description = "ID of the VM image"
}

