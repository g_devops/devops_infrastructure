locals {
  zone             = var.zone
  username         = var.username
  ssh_key_path     = var.ssh_key_path
  target_folder_id = var.target_folder_id
  registry_name    = var.registry_name
  sa_name          = var.sa_name
  network_name     = var.network_name
  subnet_name      = var.subnet_name
  vm_name          = var.vm_name
  image_id         = var.image_id
}